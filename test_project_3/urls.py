from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'test_3.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'', include('suppliers.urls', namespace='suppliers')),

    url(r'^admin/', include(admin.site.urls)),
)
