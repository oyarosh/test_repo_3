# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('suppliers', '0002_auto_20150227_1621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='fax',
            field=models.CharField(max_length=255, unique=True, null=True, verbose_name='\u0424\u0430\u043a\u0441', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='telephone',
            field=models.CharField(unique=True, max_length=255, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
            preserve_default=True,
        ),
    ]
