# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address_line_1', models.TextField(verbose_name='\u0410\u0434\u0440\u0435\u0441 1')),
                ('address_line_2', models.TextField(verbose_name='\u0410\u0434\u0440\u0435\u0441 2')),
                ('town', models.CharField(max_length=255, verbose_name='\u0413\u043e\u0440\u043e\u0434')),
                ('post_code', models.CharField(max_length=255, verbose_name='\u041f\u043e\u0447\u0442\u043e\u0432\u044b\u0439 \u0438\u043d\u0434\u0435\u043a\u0441')),
                ('telephone', models.CharField(max_length=255, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('fax', models.CharField(max_length=255, verbose_name='\u0424\u0430\u043a\u0441')),
                ('email', models.EmailField(max_length=75, verbose_name='Email')),
            ],
            options={
                'verbose_name': '\u0410\u0434\u0440\u0435\u0441',
                'verbose_name_plural': '\u0410\u0434\u0440\u0435\u0441\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Supplier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('addresses', models.ManyToManyField(to='suppliers.Address', verbose_name='\u0410\u0434\u0440\u0435\u0441\u0430')),
            ],
            options={
                'verbose_name': '\u041f\u043e\u0441\u0442\u0430\u0432\u0449\u0438\u043a',
                'verbose_name_plural': '\u041f\u043e\u0441\u0442\u0430\u0432\u0449\u0438\u043au',
            },
            bases=(models.Model,),
        ),
    ]
