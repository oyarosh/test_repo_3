# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('suppliers', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='address_line_2',
            field=models.TextField(null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441 2', blank=True),
            preserve_default=True,
        ),
    ]
