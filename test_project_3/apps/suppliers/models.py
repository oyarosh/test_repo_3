# coding: utf-8
from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.core.urlresolvers import reverse

class Address(models.Model):
    address_line_1 = models.TextField(u'Адрес 1')
    address_line_2 = models.TextField(u'Адрес 2', blank=True, null=True)
    town = models.CharField(u'Город', max_length=255)
    post_code = models.CharField(u'Почтовый индекс', max_length=255)
    telephone = models.CharField(u'Телефон', max_length=255, unique=True)
    fax = models.CharField(u'Факс', max_length=255, blank=True, 
                           null=True, unique=True)
    email = models.EmailField(u'Email')

    class Meta:
        verbose_name = u'Адрес'
        verbose_name_plural = u'Адреса'

    def __unicode__(self):
        return u'%s' % self.address_line_1
    
    def get_delete_url(self):
        return reverse('suppliers:address_delete',
                       kwargs={'pk': self.pk})


class Supplier(models.Model):
    addresses = models.ManyToManyField(Address, verbose_name = u'Адреса')
    name = models.CharField(u'Имя поставщика', max_length=255)

    class Meta:
        verbose_name = u'Поставщик'
        verbose_name_plural = u'Поставщикu'

    def __unicode__(self):
        return u'%s' % self.name

    def get_absolute_url(self):
        return reverse('suppliers:detail', kwargs={'pk': self.pk})
    

@receiver(pre_delete, sender=Supplier)
def addresses_pre_delete_handler(sender, **kwargs):
    instance = kwargs.get('instance')
    related_addresses = getattr(instance, 'addresses')
    related_addresses.all().delete()

