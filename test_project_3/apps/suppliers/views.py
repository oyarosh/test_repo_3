# coding: utf-8
from django.shortcuts import render
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.forms.models import model_to_dict
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.shortcuts import redirect, render
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.db.models import Q

from suppliers.models import Address, Supplier
from suppliers.forms import SupplierForm, AddressForm


def supplier_create(request):
    post = request.POST or  None
    supplier = SupplierForm(post)
    address = AddressForm(post)

    formset = {
        'supplier_form': supplier,
        'address_form': address,
    }

    if all(form.is_valid() for form in formset.itervalues()):
        supplier = supplier.save()
        address = address.save()
        supplier.addresses.add(address)
        return redirect('suppliers:list')

    context_data = {
        'formset': formset,
        'title': u'Создание поставщика',
    }
    return render(request, 'suppliers/suppliers_form.html', context_data)
    

class SuppliersListView(ListView):
    model = Supplier
    template_name = 'suppliers/suppliers_list.html'

    def get_context_data(self, **kwargs):
        kwargs = super(SuppliersListView, self).get_context_data(**kwargs)
        kwargs['create_supplier_url'] = reverse('suppliers:create')
        return kwargs

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            ajax_handler = getattr(self, 'handle_GET_ajax', None)
            if ajax_handler is not None:
                return ajax_handler()
            return self.http_method_not_allowed(request, *args, **kwargs)
        return super(SuppliersListView, self).get(request, *args, **kwargs)

    def handle_GET_ajax(self):
        queryset = Supplier.objects.all()
        query = self.request.GET.get('q', None)

        if query:
            queryset = queryset.filter(Q(name__icontains=query))

        rendered = render_to_string('suppliers/ajax_list.html',
                                    {'object_list': queryset})

        return JsonResponse(rendered, safe=False)


class SupplierUpdateView(UpdateView):
    model = Supplier
    template_name = 'suppliers/suppliers_form.html'
    fields = ['name']

    def get_success_url(self):
        return reverse('suppliers:detail', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        kwargs = super(SupplierUpdateView, self).get_context_data(**kwargs)
        kwargs['title'] = 'Supplier name edit'
        return kwargs
    

class SupplierDeleteView(DeleteView):
    model = Supplier
    template_name = 'suppliers/address_confirm_delete.html'

    def get_success_url(self):
        return reverse('suppliers:list')


class SupplierDetailView(DetailView):
    model = Supplier
    template_name = 'suppliers/supplier_detail.html'

    def get_context_data(self, **kwargs):
        kwargs = super(SupplierDetailView, self).get_context_data(**kwargs)
        kwargs['addresses'] = self.object.addresses.all()
        kwargs['edit_url'] = reverse('suppliers:update',
                                     kwargs={'pk': self.object.pk})
        kwargs['add_address_url'] = reverse('suppliers:address_create',
                                            kwargs={'sup_pk': self.object.pk})
        kwargs['supplier_delete_url'] = reverse('suppliers:delete',
                                                kwargs={'pk': self.object.pk})
        return kwargs


class AddressCreateView(CreateView):
    form_class = AddressForm
    template_name = 'suppliers/suppliers_form.html'
     
    def form_valid(self, form):
        self.object = form.save()
        supplier_pk = self.kwargs.get('sup_pk')
        supplier = Supplier.objects.get(pk=supplier_pk)
        supplier.addresses.add(self.object)
        return redirect(supplier)

    def get_context_data(self, **kwargs):
        kwargs = super(AddressCreateView, self).get_context_data(**kwargs)
        kwargs['title'] = 'Create address of supplier'
        return kwargs


class AddressDeleteView(DeleteView):
    model = Address

    def get(self, request, *args, **kwargs):
        supplier = self.get_object().supplier_set.all().first()
        if len(supplier.addresses.all()) == 1:
            messages.error(request, "You can't delete address. You have only 1!")
            return redirect(supplier)
        return super(AddressDeleteView, self).get(request, *args, **kwargs)

    def get_success_url(self):
        supplier = self.object.supplier_set.all().first()
        return reverse('suppliers:detail', kwargs={'pk': supplier.pk})
