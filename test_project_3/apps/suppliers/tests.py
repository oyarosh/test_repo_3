from django.test import TestCase
from django.core.urlresolvers import reverse

from suppliers.models import Address, Supplier


class SupplierTestCase(TestCase):

    def setUp(self):
        addr_1 = Address.objects.create(address_line_1='addr1',
                                        town='city',
                                        post_code='12',
                                        telephone='234',
                                        fax='234',
                                        email='patron@mailinator.com')
        addr_2 = Address.objects.create(address_line_1='ala1',
                                        address_line_2='ali2',
                                        town='newcity',
                                        telephone='233',
                                        fax='43',
                                        email='snaryad@mailinator.com')
        supplier = Supplier.objects.create(name='voinSveta')
        supplier.addresses.add(addr_1, addr_2)


    def test_list(self):
        url = reverse('suppliers:list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 1)


    def test_supplier_create(self):
        url = reverse('suppliers:create')
        data = {
            'name': 'helloword',        
            'address_line_1': 'ad1',
            'address_line_2': 'ad2',
            'town': 'kiev',
            'post_code': 2342,
            'telephone': 99999,
            'fax': 32423,
            'email': 'kaskad@asdf.com',
        }

        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.redirect_chain)
        
        supplier = Supplier.objects.get(name='helloword')
        self.assertEqual(supplier.addresses.count(), 1)

    
    def test_supplier_editing(self):
        supplier = Supplier.objects.all().first()
        url = reverse('suppliers:update', kwargs={'pk': supplier.pk})
        name = 'voinTmi'
        response = self.client.post(url, {'name': name}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.redirect_chain)
        supplier = Supplier.objects.get(pk=supplier.pk)
        self.assertEqual(supplier.name, name)

        
    def test_supplier_delete(self):
        suppliers_count = Supplier.objects.count()
        addresses_count = Address.objects.count()

        supplier = Supplier.objects.all().first()
        supplier_addresses = supplier.addresses.count()

        url = reverse('suppliers:delete', kwargs={'pk': supplier.pk})
        response = self.client.post(url, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.redirect_chain)

        self.assertEqual(suppliers_count, Supplier.objects.count() + 1)
        self.assertEqual(addresses_count,
                         Address.objects.count() + supplier_addresses)

        
    def test_address_create(self):
        supplier = Supplier.objects.all().first()
        addr_count = supplier.addresses.count()
        
        url = reverse('suppliers:address_create',
                      kwargs={'sup_pk': supplier.pk})

        data = {
            'address_line_1': 'ad1',
            'address_line_2': 'ad2',
            'town': 'kiev',
            'post_code': 2342111,
            'telephone': 9999921,
            'fax': 3242355,
            'email': 'kaskad@asdf.com',
        }

        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.redirect_chain)

        self.assertEqual(addr_count, supplier.addresses.count() - 1)


    def test_address_delete(self):
        supplier = Supplier.objects.all().first()
        address = supplier.addresses.all().first()
        addr_count = supplier.addresses.count()

        url = reverse('suppliers:address_delete', kwargs={'pk': address.pk})
        response = self.client.post(url, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.redirect_chain)

        self.assertEqual(addr_count, supplier.addresses.count() + 1)

