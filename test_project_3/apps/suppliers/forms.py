# coding: utf-8
from django import forms

from suppliers.models import Supplier, Address


class SupplierForm(forms.ModelForm):

    class Meta:
        model = Supplier
        exclude = ['addresses']


class AddressForm(forms.ModelForm):

    class Meta:
        model = Address
        fields = ['town', 'address_line_1', 'address_line_2', 'post_code',
                  'telephone', 'fax', 'email']

