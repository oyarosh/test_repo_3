from django.conf.urls import patterns, include, url

from suppliers.views import SuppliersListView, SupplierUpdateView, \
        SupplierDetailView, SupplierDeleteView, AddressDeleteView, \
        AddressCreateView


urlpatterns = patterns('suppliers.views',
        url(r'^$', SuppliersListView.as_view(), name='list'),
        url(r'^create/$', 'supplier_create', name='create'), 
        url(r'^detail/(?P<pk>\d+)/', SupplierDetailView.as_view(), name='detail'),
        url(r'^update/(?P<pk>\d+)/', SupplierUpdateView.as_view(), name='update'),
        url(r'^delete/(?P<pk>\d+)/', SupplierDeleteView.as_view(), name='delete'),
        url(r'^address_create/(?P<sup_pk>\d+)/', AddressCreateView.as_view(),
            name='address_create'),
        url(r'^address_delete/(?P<pk>\d+)/', AddressDeleteView.as_view(),
            name='address_delete'),
    )
